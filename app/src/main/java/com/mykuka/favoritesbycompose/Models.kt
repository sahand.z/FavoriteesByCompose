package com.mykuka.favoritesbycompose

data class FavoriteItemModel(val title: String)
data class SuggestionItemModel(
    val price: String,
    val serviceType: String,
    val origin: String,
    val destination: String
)