package com.mykuka.favoritesbycompose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.mykuka.favoritesbycompose.ui.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FavoritesByComposeTheme {
                Surface(color = MaterialTheme.colors.background) {
                    FavoriteScreen()
                }
            }
        }
    }
}

@Composable
fun FavoriteScreen(modifier: Modifier = Modifier) {
    Column() {
        val scrollState = rememberLazyListState()
        Row {
            FavoriteScreenHeader(
                screenTitle = LocalContext.current.getString(R.string.favorite_routes),
                shouldShowShadow = scrollState.firstVisibleItemIndex != 0
            )
        }
        Row(
            modifier = modifier
                .weight(1f)
                .padding(16.dp, 0.dp)
        ) {
            SmartPreviewList(lazyListStateRemember = scrollState)
        }
        if (!scrollState.isScrolledToTheEnd()) {
            Row(
                modifier = modifier
                    .fillMaxWidth()
                    .background(Color.Gray)
                    .height(0.5.dp)
            ) {
            }
        }
        Row() {
            AddFavoriteLayout()
        }
    }

}

@Composable
@Preview
fun DefaultPreview() {
    FavoritesByComposeTheme {
        FavoriteScreen()
    }
}

fun LazyListState.isScrolledToTheEnd() =
    layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1
