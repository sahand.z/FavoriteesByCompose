package com.mykuka.favoritesbycompose.ui

import androidx.compose.ui.graphics.Color


val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)
val subtitle = Color(0xff4d4d4d)
val orange = Color(0xFFFFE6DE)
val darkOrange = Color(0xFFFF5722)
val disabledText = Color(0xFF4d4d4d)
val lightWhite = Color(0xFFf1f1f1)