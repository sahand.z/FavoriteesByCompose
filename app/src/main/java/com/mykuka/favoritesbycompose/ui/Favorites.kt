package com.mykuka.favoritesbycompose.ui

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Gray
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.mykuka.favoritesbycompose.FavoriteItemModel
import com.mykuka.favoritesbycompose.R
import com.mykuka.favoritesbycompose.SuggestionItemModel

@Composable
fun FavoriteListTitle(
    text: String,
    modifier: Modifier = Modifier,
) {
    Text(text = text, modifier = modifier, color = Black, fontSize = 14.sp)
}

@Composable
fun FavoriteScreenHeader(
    modifier: Modifier = Modifier,
    onItemClicked: () -> Unit = {},
    screenTitle: String,
    shouldShowShadow: Boolean
) {
    Row(
        horizontalArrangement = Arrangement.End,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .height(56.dp)
            .shadow(elevation = if (shouldShowShadow) 2.dp else 0.dp)
            .padding(16.dp, 0.dp)
    ) {
        Text(text = screenTitle, modifier = modifier.padding(4.dp, 0.dp))

        Image(
            modifier = modifier.clickable { onItemClicked() },
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_arrow_right),
            contentDescription = "back button"
        )
    }

}

@Preview
@Composable
fun SmartPreviewList(
    modifier: Modifier = Modifier,
    onItemClicked: () -> Unit = {},
    lazyListStateRemember: LazyListState
) {
    LazyColumn(
        state = lazyListStateRemember
    ) {
        item() {
            SmartPreviewTitleItem(
                modifier
                    .padding(0.dp, 8.dp, 0.dp, 8.dp)
                    .height(24.dp),

                LocalContext.current.getString(R.string.tapsi_sugestion)
            )
        }

        items(3) {
            SuggestionItemLayout(
                Modifier,
                SuggestionItemModel(
                    "26,000",
                    origin = "تپسی",
                    destination = "خونه",
                    serviceType = "کلاسک"
                )
            )
        }

        item {
            SmartPreviewTitleItem(
                modifier
                    .padding(0.dp, 8.dp, 0.dp, 8.dp)
                    .height(24.dp),
                LocalContext.current.getString(R.string.favorite_places)
            )
        }

        items(20) {

            FavoriteItemLayout(Modifier, FavoriteItemModel("مکان منتخب " + it))
        }
    }

}

@Composable
private fun SmartPreviewTitleItem(modifier: Modifier, title: String) {
    Text(
        text = title,
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp, 0.dp, 0.dp, 0.dp),
        textAlign = TextAlign.End,
        color = subtitle,
        fontSize = 14.sp,
        fontWeight = FontWeight.Bold,

        )
}

@Composable
fun FavoriteItemLayout(
    modifier: Modifier = Modifier,
    item: FavoriteItemModel,
    onItemClicked: () -> Unit = {},
) {
    Row(
        modifier = modifier
            .padding(0.dp, 8.dp)
            .fillMaxWidth()
            .height(48.dp)
            .clip(RoundedCornerShape(12.dp))
            .border(BorderStroke(0.5.dp, Gray), shape = RoundedCornerShape(12.dp))
            .clickable { onItemClicked }
            .padding(8.dp, 0.dp, 16.dp, 0.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_arrow_orange),
            contentDescription = "take Tapsi"
        )
        Text(
            text = item.title,
            modifier = modifier
                .weight(1f)
                .padding(8.dp, 0.dp),
            textAlign = TextAlign.End
        )
        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_recent_fav),
            contentDescription = "favoriteImage"
        )

    }
}

@Composable
fun SuggestionItemLayout(
    modifier: Modifier = Modifier,
    item: SuggestionItemModel,
    onItemClicked: () -> Unit = {},
) {
    @Composable
    fun AddressTitle(placeName: String, description: String) {
        Row {
            Text(
                text = placeName,
                modifier = modifier
                    .weight(1f)
                    .padding(4.dp, 0.dp),
                color = Black,
                textAlign = TextAlign.End
            )
            Text(
                text = description,
                color = disabledText
            )
        }
    }

    Row(
        modifier = modifier
            .padding(0.dp, 8.dp)
            .fillMaxWidth()
            .height(80.dp)
            .clip(RoundedCornerShape(12.dp))
            .border(BorderStroke(0.5.dp, Gray), shape = RoundedCornerShape(12.dp))
            .clickable { onItemClicked }
            .padding(8.dp, 0.dp, 16.dp, 0.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_arrow_orange),
            contentDescription = "take Tapsi"
        )
        Column() {
            Row {
                Text(text = item.price, fontSize = 14.sp, color = disabledText)
            }
            Row {
                Text(
                    text = item.serviceType,
                    modifier
                        .clip(RoundedCornerShape(12.dp))
                        .background(lightWhite)
                        .padding(4.dp)
                        .height(18.dp), fontSize = 12.sp, color = disabledText
                )
            }
        }

        Column(
            modifier = modifier
                .weight(1f)
                .padding(8.dp, 0.dp)
        ) {
            AddressTitle (item.origin, LocalContext.current.getString(R.string.from))
            AddressTitle (item.destination, LocalContext.current.getString(R.string.to))
        }

        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_recent_fav),
            contentDescription = "favoriteImage"
        )
    }
}


@Composable
fun AddFavoriteButton(
    modifier: Modifier = Modifier,
    onItemClicked: () -> Unit = {},
) {
    Button(
        modifier = modifier
            .height(52.dp)
            .width(52.dp)
            .clip(RoundedCornerShape(12.dp)),
        onClick = onItemClicked,
        colors = ButtonDefaults.buttonColors(contentColor = darkOrange, backgroundColor = orange)

    ) {
        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_baseline_add_24),
            contentDescription = null,
        )
    }
}

@Composable
fun AddFavoriteLayout(
    modifier: Modifier = Modifier,
    onItemClicked: () -> Unit = {},
) {

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .clip(RoundedCornerShape(12.dp))
            .clickable { onItemClicked }
            .padding(16.dp, 0.dp)
    ) {
        Column(
            modifier = modifier
                .weight(1f)
                .padding(8.dp, 8.dp)
                .height(52.dp),
        ) {
            Text(
                text = LocalContext.current.getString(R.string.add_new_address),
                modifier = modifier.fillMaxWidth(),
                textAlign = TextAlign.End
            )
            Text(
                text = LocalContext.current.getString(R.string.add_new_address_desc),
                modifier = modifier.fillMaxWidth(),
                textAlign = TextAlign.End,
                color = disabledText,
            )
        }

        AddFavoriteButton()

    }

}



